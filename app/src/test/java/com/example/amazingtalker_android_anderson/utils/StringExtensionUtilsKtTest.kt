package com.example.amazingtalker_android_anderson.utils

import com.example.amazingtalker_android_anderson.constants.GeneralConstants
import org.junit.Assert
import org.junit.Test

class StringExtensionUtilsKtTest {
    var invalidTestString = listOf("", "abc123", "!@#", "中文")

    @Test
    fun `test parsing Date String To Date within Format`() {
        val convertedList = invalidTestString.map { testInput ->
            testInput.parseDateStringToDateByFormat(GeneralConstants.YMD_FORMAT)
        }

        Assert.assertEquals(convertedList.size, invalidTestString.size)
    }
}