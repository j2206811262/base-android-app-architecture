package com.example.amazingtalker_android_anderson

import android.os.Bundle
import com.example.amazingtalker_android_anderson.base.BaseActivity
import com.example.amazingtalker_android_anderson.databinding.ActivityMainBinding
import com.example.amazingtalker_android_anderson.fragment.MainFragment
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.HiltAndroidApp

@AndroidEntryPoint
class MainActivity : BaseActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        changeFragment(binding.replaceFrameLayout.id, MainFragment.newInstance())
    }
}