package com.example.amazingtalker_android_anderson.remote.dto

data class ScheduleDTO(
    val available: List<StartEndTimeString>,
    val booked: List<StartEndTimeString>
)

data class StartEndTimeString(
    val end: String,
    val start: String
)