package com.example.amazingtalker_android_anderson.remote.repository

import com.example.amazingtalker_android_anderson.remote.datasource.ScheduleRemoteSource
import javax.inject.Inject

class ScheduleRepository @Inject constructor(
    private val scheduleRemoteSource: ScheduleRemoteSource
) {
    suspend fun getScheduleByTeacherName(
        teacherName: String,
        startedAt: String
    ) = scheduleRemoteSource.getScheduleByTeacherName(teacherName, startedAt)
}