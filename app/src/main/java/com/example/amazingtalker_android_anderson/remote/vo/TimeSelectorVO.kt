package com.example.amazingtalker_android_anderson.remote.vo

import java.util.*

data class ScheduleDate(
    val dateTime: Date,
    val availablePeriodList: ArrayList<Period> = arrayListOf()
)

data class Period(
    val periodType: PeriodType,
    val availableTimeList: List<TimeItem>
)

data class TimeItem(
    val dateTime: Date,
    val isAvailable: Boolean
)

enum class PeriodType(val typeId: Int, val typeName: String) {
    MORNING(0, "Morning"),
    EVENING(1, "Evening"),
    NIGHT(2, "Night")
}