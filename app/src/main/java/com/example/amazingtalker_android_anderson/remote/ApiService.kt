package com.example.amazingtalker_android_anderson.remote

import com.example.amazingtalker_android_anderson.remote.dto.ScheduleDTO
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    companion object {
        private const val GUEST_CONTROLLER = "v1/guest"
    }

    @GET("$GUEST_CONTROLLER/teachers/{teacherName}/schedule")
    suspend fun getScheduleByTeacherName(
        @Path ("teacherName") teacherName: String,
        @Query("started_at") startedAt: String
    ): ScheduleDTO
}