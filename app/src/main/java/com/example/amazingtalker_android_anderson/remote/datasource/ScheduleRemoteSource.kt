package com.example.amazingtalker_android_anderson.remote.datasource

import com.example.amazingtalker_android_anderson.remote.ApiService
import retrofit2.http.Path
import retrofit2.http.Query
import javax.inject.Inject

class ScheduleRemoteSource @Inject constructor(
    private val apiService: ApiService
){
    suspend fun getScheduleByTeacherName(
        teacherName: String,
        startedAt: String
    ) = apiService.getScheduleByTeacherName(teacherName, startedAt)
}