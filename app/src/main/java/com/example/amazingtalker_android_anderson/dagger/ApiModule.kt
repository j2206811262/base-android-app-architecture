package com.example.amazingtalker_android_anderson.dagger

import com.example.amazingtalker_android_anderson.remote.ApiService
import com.example.amazingtalker_android_anderson.dagger.ApiModuleUtil.provideApiService
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.internal.managers.ApplicationComponentManager
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class ApiModule {
    @Singleton
    @Provides
    fun provideGeneralApiService(
        okHttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory
    ) = provideApiService(okHttpClient, converterFactory, ApiService::class.java)

    @Provides
    @Singleton
    fun provideGSON(): Gson = Gson()

    @Provides
    @Singleton
    fun provideGSONConverterFactory(gson: Gson): GsonConverterFactory =
        GsonConverterFactory.create(gson)

    @Provides
    fun provideOkHttpClient(
    ): OkHttpClient =
        getOkHttpClient().build()

    private fun getOkHttpClient() =
        OkHttpClient.Builder()
            .addInterceptor (HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
}