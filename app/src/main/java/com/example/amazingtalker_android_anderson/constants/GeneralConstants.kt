package com.example.amazingtalker_android_anderson.constants

object GeneralConstants {
    const val TIME_STAMP_FORMAT = "yyyy-MM-dd'T'hh:mm:ss.SSS'Z'"
    const val YMD_FORMAT = "yyyy-MM-dd"
    const val DAY_NAME_FORMAT = "EEE,MMM dd"
    const val MD_FORMAT = "MM-dd"
    const val YMD_T_HM_FORMAT = "yyyy-MM-dd'T'hh:mm"
    const val HM_FORMAT = "hh:mm"
}