package com.example.amazingtalker_android_anderson.utils

import com.example.amazingtalker_android_anderson.constants.GeneralConstants
import com.example.amazingtalker_android_anderson.remote.vo.PeriodType
import java.text.SimpleDateFormat
import java.util.*

const val NUM_DAYS_IN_WEEK = 7
fun Date.getWeekTypeTitle(): String {
    val calendarInstance = Calendar.getInstance().initTime(this)

    val startAt = getSimpleDateFormat(GeneralConstants.YMD_FORMAT)
        .format(calendarInstance.time)

    calendarInstance.add(Calendar.DAY_OF_YEAR, NUM_DAYS_IN_WEEK)

    val endAt = getSimpleDateFormat(GeneralConstants.MD_FORMAT)
        .format(calendarInstance.time)
    return StringBuilder().append(startAt).append("-").append(endAt).toString()
}

fun Date.addDay(numDays: Int): Date {
    val calendarInstance = Calendar.getInstance().initTime(this)
    calendarInstance.add(Calendar.DAY_OF_WEEK, numDays)
    return calendarInstance.time
}

fun Date.parseWithFormatToString(targetFormat: String): String {
    return getSimpleDateFormat(targetFormat).format(this).toString()
}


fun Date.convertToPeriodType(): PeriodType {
    val hourFormatter = getSimpleDateFormat("HH")
    val hour = hourFormatter.format(this).toString().toInt()
    return when (hour) {
        in 1..11 -> {
            PeriodType.MORNING
        }
        in 13..17 -> {
            PeriodType.EVENING
        }
        else -> {
            PeriodType.NIGHT
        }
    }
}

private fun getSimpleDateFormat(format: String): SimpleDateFormat {
    val simpleDateFormat = SimpleDateFormat(format)
    simpleDateFormat.timeZone = TimeZone.getTimeZone("GMT 08")
    return simpleDateFormat
}