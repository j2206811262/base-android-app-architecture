package com.example.amazingtalker_android_anderson.utils

import java.util.*

fun Calendar.initTime(date: Date): Calendar {
    time = date
    return this
}