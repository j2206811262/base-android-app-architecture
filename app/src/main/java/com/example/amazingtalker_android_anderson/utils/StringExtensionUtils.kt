package com.example.amazingtalker_android_anderson.utils

import java.text.SimpleDateFormat
import java.util.*

fun String.parseDateStringToDateByFormat(format: String): Date {
    return try {
        val date = SimpleDateFormat(format, Locale.getDefault()).parse(this)
        date ?: Date()
    } catch (parseException: Exception) {
        Date()
    }
}