package com.example.amazingtalker_android_anderson.adapter

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.amazingtalker_android_anderson.R
import com.example.amazingtalker_android_anderson.constants.GeneralConstants
import com.example.amazingtalker_android_anderson.databinding.TimeSelectionItemBinding
import com.example.amazingtalker_android_anderson.remote.vo.TimeItem
import com.example.amazingtalker_android_anderson.utils.parseWithFormatToString

class TimeItemAdapter(private val context: Context) :
    RecyclerView.Adapter<TimeItemAdapter.TimeItemViewHolder>() {
    private lateinit var binding: TimeSelectionItemBinding
    private var timeItemList = listOf<TimeItem>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TimeItemViewHolder {
        binding =
            TimeSelectionItemBinding.inflate(LayoutInflater.from(context), parent, false)
        return TimeItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TimeItemViewHolder, position: Int) {
        holder.setData(timeItemList[position])
    }

    override fun getItemCount(): Int {
        return timeItemList.size
    }

    fun setTimeItem(timeItemList: List<TimeItem>) {
        this.timeItemList = timeItemList
    }


    class TimeItemViewHolder(private val binding: TimeSelectionItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setData(availableTime: TimeItem) {
            with(binding.selectionItemNameTextView) {
                text = availableTime.dateTime.parseWithFormatToString(GeneralConstants.HM_FORMAT)
                isSelected = !availableTime.isAvailable
                if (isSelected) {
                    setTypeface(null, Typeface.BOLD)
                    setTextColor(ContextCompat.getColor(context, R.color.black))
                } else {
                    setTypeface(null, Typeface.NORMAL)
                    ContextCompat.getColor(context, R.color.color_02DF82)
                }

                setOnClickListener {
                    val displayContent = StringBuilder()
                        .append(availableTime.dateTime)
                        .append(" clicked!")
                        .toString()
                    Toast.makeText(context, displayContent, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}