package com.example.amazingtalker_android_anderson.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.amazingtalker_android_anderson.databinding.AvailablePeriodFragmentBinding
import com.example.amazingtalker_android_anderson.remote.vo.ScheduleDate

class SelectTimeViewPagerAdapter(private val context: Context) :
    RecyclerView.Adapter<SelectTimeViewPagerAdapter.SelectTimeViewHolder>() {
    lateinit var binding: AvailablePeriodFragmentBinding
    private var periodData: List<ScheduleDate> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectTimeViewHolder {
        binding =
            AvailablePeriodFragmentBinding.inflate(LayoutInflater.from(context), parent, false)
        return SelectTimeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SelectTimeViewHolder, position: Int) {
        holder.setData(periodData[position])
    }

    override fun getItemCount(): Int {
        return periodData.size
    }

    fun setPeriodData(periodData: ArrayList<ScheduleDate>) {
        this.periodData = periodData
    }

    class SelectTimeViewHolder(private val binding: AvailablePeriodFragmentBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setData(availableDate: ScheduleDate) {
            binding.periodRecyclerView.apply {
                layoutManager = LinearLayoutManager(context)
                val timeListAdapter = AvailablePeriodAdapter(context)
                adapter = timeListAdapter
                timeListAdapter.setPeriodList(availableDate.availablePeriodList)
            }
        }
    }
}