package com.example.amazingtalker_android_anderson.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.amazingtalker_android_anderson.databinding.PeriodSelectionItemBinding
import com.example.amazingtalker_android_anderson.remote.vo.Period

class AvailablePeriodAdapter(private val context: Context) :
    RecyclerView.Adapter<AvailablePeriodAdapter.PeriodViewHolder>() {
    private val periodList = arrayListOf<Period>()
    private lateinit var binding: PeriodSelectionItemBinding

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PeriodViewHolder {
        binding =
            PeriodSelectionItemBinding.inflate(LayoutInflater.from(context), parent, false)
        return PeriodViewHolder(binding)
    }

    override fun onBindViewHolder(
        holder: PeriodViewHolder,
        position: Int
    ) {
        holder.setData(periodList[position])
    }

    override fun getItemCount(): Int {
        return periodList.size
    }

    fun setPeriodList(periodList: ArrayList<Period>) {
        this.periodList.clear()
        this.periodList.addAll(periodList)
    }


    class PeriodViewHolder(private val binding: PeriodSelectionItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val timeItemAdapter = TimeItemAdapter(binding.root.context)
        fun setData(period: Period) {
            binding.titleTextView.text = period.periodType.name
            binding.timeSelectionRecyclerView.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = timeItemAdapter
                timeItemAdapter.setTimeItem(period.availableTimeList)
            }
        }
    }
}