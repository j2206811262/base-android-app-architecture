package com.example.amazingtalker_android_anderson.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.amazingtalker_android_anderson.databinding.FragmentMainBinding
import com.example.amazingtalker_android_anderson.viewmodel.MainFragmentViewModel
import com.example.amazingtalker_android_anderson.widget.timeselector.TimeSelectorWeekly
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class MainFragment : Fragment(), TimeSelectorWeekly.UpdateTimeRangeCallBack {
    companion object {
        const val TAG = "MainFragment"
        const val TEST_TEACHER_NAME = "amy-estrada"
        @JvmStatic
        fun newInstance() = MainFragment().apply {}
    }

    private lateinit var binding: FragmentMainBinding
    private val mainFragmentViewModel: MainFragmentViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = FragmentMainBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.timeSelectorWeekly.addOnTimeRangeUpdatedCallback(this)
        binding.timeSelectorWeekly.initStartDate(Date())
    }

    override fun timeRangeUpdated(forwardedDate: Date) {
        mainFragmentViewModel.getScheduleData(
            TEST_TEACHER_NAME, forwardedDate
        ).observe(viewLifecycleOwner) { scheduleDTO ->
            val dayList = mainFragmentViewModel.convertToDayList(scheduleDTO)
            binding.timeSelectorDaily.setDayList(dayList)
        }
    }
}