package com.example.amazingtalker_android_anderson.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.example.amazingtalker_android_anderson.constants.GeneralConstants
import com.example.amazingtalker_android_anderson.constants.GeneralConstants.TIME_STAMP_FORMAT
import com.example.amazingtalker_android_anderson.remote.dto.ScheduleDTO
import com.example.amazingtalker_android_anderson.remote.dto.StartEndTimeString
import com.example.amazingtalker_android_anderson.remote.repository.ScheduleRepository
import com.example.amazingtalker_android_anderson.remote.vo.Period
import com.example.amazingtalker_android_anderson.remote.vo.ScheduleDate
import com.example.amazingtalker_android_anderson.remote.vo.TimeItem
import com.example.amazingtalker_android_anderson.utils.convertToPeriodType
import com.example.amazingtalker_android_anderson.utils.parseDateStringToDateByFormat
import com.example.amazingtalker_android_anderson.utils.parseWithFormatToString
import dagger.hilt.android.lifecycle.HiltViewModel
import java.util.*
import javax.inject.Inject

@HiltViewModel
class MainFragmentViewModel @Inject constructor(
    private val scheduleRepository: ScheduleRepository
) : ViewModel() {
    fun getScheduleData(teacherName: String, startedDate: Date): LiveData<ScheduleDTO> {
        val timeStamp = startedDate.parseWithFormatToString(TIME_STAMP_FORMAT)
        return liveData(viewModelScope.coroutineContext) {
            emit(scheduleRepository.getScheduleByTeacherName(teacherName, timeStamp))
        }
    }

    fun convertToDayList(scheduleDTO: ScheduleDTO): ArrayList<ScheduleDate> {
        val dayMap = HashMap<Date, ArrayList<Period>>()
        scheduleDTO.available.map {
            convertDateTimeStringToScheduleDateVO(it, scheduleDTO.available, dayMap, true)
        }
        scheduleDTO.booked.map {
            convertDateTimeStringToScheduleDateVO(it, scheduleDTO.booked, dayMap, false)
        }

        return convertDayMapToVO(dayMap)
    }

    private fun convertDayMapToVO(dayMap: HashMap<Date, ArrayList<Period>>): ArrayList<ScheduleDate> {
        val result = arrayListOf<ScheduleDate>()
        for (key in dayMap.keys) {
            result.add(
                ScheduleDate(
                    key,
                    dayMap[key]!!
                )
            )
        }
        result.sortBy { it.dateTime }
        return result
    }

    private fun convertDateTimeStringToScheduleDateVO(
        sourceDateTimeString: StartEndTimeString,
        allTodayDateList: List<StartEndTimeString>,
        dayMap: HashMap<Date, ArrayList<Period>>,
        isAvailable: Boolean
    ) {
        val availableDateTime =
            sourceDateTimeString.start.parseDateStringToDateByFormat(GeneralConstants.YMD_T_HM_FORMAT)
        val period = Period(
            availableDateTime.convertToPeriodType(),
            getSamePeriodTimeItem(allTodayDateList, sourceDateTimeString, isAvailable),
        )

        val dateStrYMDFormatKey =
            sourceDateTimeString.start.parseDateStringToDateByFormat(GeneralConstants.YMD_FORMAT)
        if (dayMap.containsKey(dateStrYMDFormatKey)) {
            val tempList = dayMap[dateStrYMDFormatKey]
            tempList!!.add(period)
            dayMap[dateStrYMDFormatKey] = tempList
        } else {
            dayMap[dateStrYMDFormatKey] = arrayListOf(period)
        }

        for (key in dayMap.keys) {
            dayMap[key]!!.sortBy { it.periodType.typeId }
        }
    }

    private fun getSamePeriodTimeItem(
        allTodayDateList: List<StartEndTimeString>,
        target: StartEndTimeString,
        isAvailable: Boolean
    ): List<TimeItem> {
        val dateList = allTodayDateList.map {
            it.start.parseDateStringToDateByFormat(GeneralConstants.YMD_T_HM_FORMAT)
        }

        val targetDate =
            target.start.parseDateStringToDateByFormat(GeneralConstants.YMD_T_HM_FORMAT)
        return dateList.filter { date ->
            date == targetDate
        }.map { filteredDate ->
            TimeItem(
                filteredDate,
                isAvailable
            )
        }

    }
}