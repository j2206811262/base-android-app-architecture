package com.example.amazingtalker_android_anderson.widget.timeselector

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.example.amazingtalker_android_anderson.R
import com.example.amazingtalker_android_anderson.databinding.TimeSelectorWeeklyComponentBinding
import com.example.amazingtalker_android_anderson.utils.addDay
import com.example.amazingtalker_android_anderson.utils.getWeekTypeTitle
import java.util.*

class TimeSelectorWeekly @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    companion object {
        const val DAYS_OF_WEEK = 7
    }

    private var binding: TimeSelectorWeeklyComponentBinding =
        TimeSelectorWeeklyComponentBinding.inflate(LayoutInflater.from(context), this, true)

    private var forwardWeekCount = 0
    private var startedDate = Date()
    private var updateTimeRangeCallBack: UpdateTimeRangeCallBack? = null

    init {
        binding.rightArrowImageView.setOnClickListener {
            forwardWeekCount++
            val forwardedDate = startedDate.addDay(forwardWeekCount * DAYS_OF_WEEK)
            updateTimeRange(forwardedDate)
        }

        binding.leftArrowImageView.setOnClickListener {
            if (forwardWeekCount <= 0) {
                forwardWeekCount = 0
            } else {
                forwardWeekCount--
                val forwardedDate = startedDate.addDay(forwardWeekCount * DAYS_OF_WEEK)
                updateTimeRange(forwardedDate)
            }
        }
    }

    private fun updateTimeRange(forwardedDate: Date) {
        binding.weekRangeTextView.text = forwardedDate.getWeekTypeTitle()
        updateTimeRangeCallBack?.timeRangeUpdated(forwardedDate)

        if (forwardWeekCount == 0) {
            binding.leftArrowImageView.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.black
                )
            )
        } else {
            binding.leftArrowImageView.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.color_00BB00
                )
            )
        }
        binding.rightArrowImageView.setColorFilter(
            ContextCompat.getColor(
                context,
                R.color.color_00BB00
            )
        )
    }

    fun addOnTimeRangeUpdatedCallback(updateTimeRangeCallBack: UpdateTimeRangeCallBack) {
        this.updateTimeRangeCallBack = updateTimeRangeCallBack
    }

    fun initStartDate(date: Date) {
        startedDate = date
        forwardWeekCount = 0
        updateTimeRange(startedDate)
    }

    interface UpdateTimeRangeCallBack {
        fun timeRangeUpdated(forwardedDate: Date)
    }
}