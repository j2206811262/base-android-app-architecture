package com.example.amazingtalker_android_anderson.widget.timeselector

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.amazingtalker_android_anderson.adapter.SelectTimeViewPagerAdapter
import com.example.amazingtalker_android_anderson.constants.GeneralConstants
import com.example.amazingtalker_android_anderson.databinding.TimeSelectorDailyComponentBinding
import com.example.amazingtalker_android_anderson.remote.vo.ScheduleDate
import com.example.amazingtalker_android_anderson.utils.parseWithFormatToString
import com.google.android.material.tabs.TabLayoutMediator

class TimeSelectorDaily @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var binding: TimeSelectorDailyComponentBinding =
        TimeSelectorDailyComponentBinding.inflate(LayoutInflater.from(context), this, true)

    private var selectTimeViewPagerAdapter = SelectTimeViewPagerAdapter(context)

    fun setDayList(availableDateList: ArrayList<ScheduleDate>) {
        selectTimeViewPagerAdapter.setPeriodData(availableDateList)
        binding.availableTimeViewPager.adapter = selectTimeViewPagerAdapter

        binding.dayNamesTabLayout.removeAllTabs()
        TabLayoutMediator(
            binding.dayNamesTabLayout,
            binding.availableTimeViewPager
        ) { tab, position ->
            if (availableDateList.size > position) {
                tab.text = availableDateList[position].dateTime
                    .parseWithFormatToString(GeneralConstants.DAY_NAME_FORMAT)
            }
        }.attach()
    }
}