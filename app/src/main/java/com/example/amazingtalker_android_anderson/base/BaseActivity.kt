package com.example.amazingtalker_android_anderson.base

import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

open class BaseActivity : AppCompatActivity(){

    protected fun changeFragment(replaceLayoutId: Int, fragment: Fragment) {
        changeFragment(replaceLayoutId, fragment, null)
    }

    protected fun changeFragment(replaceLayoutId: Int, fragment: Fragment, tag: String?) {
        val fragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        if (TextUtils.isEmpty(tag)) {
            fragmentTransaction.replace(replaceLayoutId, fragment)
        } else {
            fragmentTransaction.replace(replaceLayoutId, fragment, tag)
        }
        fragmentTransaction.addToBackStack(fragment.javaClass.name)
        fragmentTransaction.commitAllowingStateLoss()
    }

}